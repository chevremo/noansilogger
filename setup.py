#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 
# This file is part of the noansilogger software (https://gitlab.esrf.fr/chevremo/noansilogger/).
# Copyright (c) 2023 William Chèvremont <william.chevremont@esrf.fr>.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from setuptools import setup, find_packages

setup(name='noansilogger',
      version='0.2',
      description='Write ANSI-removed text to file. Take care of cursor position.',
      author='William Chevremont',
      author_email='william.chevremont@esrf.fr',
      install_requires=[
      ],
      packages=find_packages(include=['noansilogger','noansilogger.*']),
      entry_points={
                    'console_scripts': [
              'noansilogger=noansilogger.noansilogger:main',
              ]
         }
    )
