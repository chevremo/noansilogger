#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 
# This file is part of the noansilogger software (https://gitlab.esrf.fr/chevremo/noansilogger/).
# Copyright (c) 2023 William Chèvremont <william.chevremont@esrf.fr>.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import sys
import logging

from .buffer import BufferWithCursor

logging.basicConfig(format='%(levelname)s %(name)s:%(lineno)d %(message)s',
                    handlers=[logging.StreamHandler(sys.stderr)])
logger = logging.getLogger(__name__)

class BufferizedNoANSIFile(object):
    """
    Contain an output file with a buffer. Remove ANSI escaped sequences
    """
    
    def __init__(self, filename, append=True, bufsize=200, keepcolors=False):
        
        self.__filename = filename
        self.__append = append
        self.__keepcolors = keepcolors
        
        self.__buffer = BufferWithCursor(bufsize)
        self.__fd = None
        
    def __del__(self):
        self.close()
        
    def __enter__(self):
        self.open()
        return self
        
    def __exit__(self, t, v, tbk):
        self.close()
        
    def open(self):
        if self.__fd is None:
            self.__fd = open(self.__filename, 'a' if self.__append else 'w')
            
    def close(self):
        if self.__fd is not None:
            
            # First recover remaining lines
            self.__fd.writelines(self.__buffer.getlines(False))
            
            self.__fd.close()
            
            self.__fd = None
            
            
    def write(self, l : str):
        
        ansi = []
        is_escaped = False
        curransi = ''
        
        for c in l:
            if c == '\033': # Detect the ANSI escaped codes
                is_escaped = True
                
            if is_escaped:
                curransi += c
                
                #CSI
                if len(curransi) >= 2 and curransi[1] == '[':
                
                    if c.lower() in 'abcdefghijklmnopqrstuvwxyz': # End of ANSI code
                        is_escaped = False
                        
                        vv = curransi[2:-1]
                        
                        if vv.startswith('?'):
                            questionmark = True
                            vv = vv[1:]
                        else:
                            questionmark = False
                        
                        vals = []
                        
                        for v in vv.split(';'):
                            v = v.strip()
                            if len(v) > 0:
                                vals += [v,]
                        
                        if c == "A":
                            # CURSOR UP
                            n  = 1
                            
                            if len(vals) > 0:
                                n = int(vals[0])
                                
                            self.__buffer.move_relative(row=n)
                        
                        elif c == "B":
                            # CURSOR DOWN
                            n  = 1
                            
                            if len(vals) > 0:
                                n = int(vals[0])
                                
                            self.__buffer.move_relative(row=-n)
                        
                        elif c == "C":
                            # CURSOR FORWARD
                            n  = 1
                            
                            if len(vals) > 0:
                                n = int(vals[0])
                                
                            self.__buffer.move_relative(col=n)
                            
                        elif c == "D":
                            # CURSOR BACKWARD
                            n  = 1
                            
                            if len(vals) > 0:
                                n = int(vals[0])
                                
                            self.__buffer.move_relative(col=-n)
                        
                        elif c == "E":
                            # CURSOR NEXT LINE
                            n  = 1
                            
                            if len(vals) > 0:
                                n = int(vals[0])
                                
                            self.__buffer.move_relative(row=-n)
                            self.__buffer.move_absolute(col=0)
                        
                        elif c == "F":
                            # CURSOR PRECEEDING LINE
                            n  = 1
                            
                            if len(vals) > 0:
                                n = int(vals[0])
                                
                            self.__buffer.move_relative(row=n)
                            self.__buffer.move_absolute(col=0)
                        
                        elif c == "G":
                            # CURSOR ABSOLUTE COLUMN
                            n  = 1
                            
                            if len(vals) > 0:
                                n = int(vals[0])
                                
                            self.__buffer.move_absolute(col=n-1)
                        
                        elif c == "H":
                            # CURSOR ABSOLUTE ROW COLUMN
                            logger.warn("H not yet implemented. Just move col but not row.")
                            
                            c, r  = 1,1
                            
                            if len(vals) > 0:
                                r = int(vals[0])
                                
                            if len(vals) > 0:
                                c = int(vals[0])
                                
                            self.__buffer.move_absolute(col=c-1)
                            
                        
                        elif c == "K":
                            # ERASE IN LINE
                            n  = 0
                            
                            if len(vals) > 0:
                                n = int(vals[0])
                                
                            self.__buffer.inline_erase(n)
                            
                        elif c == "J":
                            # ERASE screen: just make sure we are at the end of the buffer
                            n  = 0
                            
                            if len(vals) > 0:
                                n = int(vals[0])
                            
                            self.__buffer.erase_display(n)
                        
                        elif c == "m":
                            # Text formatting: add an option to keep coloring?
                            if self.__keepcolors:
                                self.__buffer.write([curransi,])
                                
                        elif c in ('n','h','l'):
                            # Ignore these codes
                            pass
                        
                        else:
                            ansi += [ c, ]
                            
                        curransi = ''
                    
                # This is OSC... just ignore.
                elif len(curransi) >= 2 and curransi[1] == ']':
                    if c in ('\x9c', '\x07'):
                        is_escaped = False
                        curransi = ''
                elif len(curransi) >= 2:
                    raise ValueError(f"Unknown escape sequence {[curransi,]}.")
                    
            else:
                if c not in ('\x07', '\x08', '\x0F'): # Ignore some chars
                    self.__buffer.write(c)
            
        if len(ansi) > 0:
            logger.warn(f"Unprocessed ANSI codes: {ansi}")

        r = self.__fd.writelines(self.__buffer.getlines())
        self.__fd.flush()
        return r

