#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 
# This file is part of the noansilogger software (https://gitlab.esrf.fr/chevremo/noansilogger/).
# Copyright (c) 2023 William Chèvremont <william.chevremont@esrf.fr>.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import sys

import logging
logging.basicConfig(format='%(levelname)s %(name)s:%(lineno)d %(message)s',
                    handlers=[logging.StreamHandler(sys.stderr)])
logger = logging.getLogger(__name__)

class BufferWithCursor(object):
    
    
    def __init__(self, nlines):
        """
        Initialize a buffer, where a curcor can move and erase text
        
        :param nlines: Number of lines to bufferize. Lines older are considered final.
        """
        
        self.__nlines = nlines
        self.__buffer = [['',]*nlines]
        self.__cursor = [0,0] # row, col
        
        
    @property
    def row(self):
        return self.__cursor[0]
    
    @property
    def col(self):
        return self.__cursor[1]
        
    def move_relative(self, row=0, col=0):
        """
        Move the cursor incrementally
        """
        self.move_absolute(self.__cursor[0]+row, self.__cursor[1]+col)
    
    
    def move_absolute(self, row=None, col=None):
        """
        Move the cursor to an absolute position.
        0 is the most recent line and 0 is the first column.
        
        :param row: Move to the nth older row. If None, don't change row.
        :param col: Move to the nth col. If None, don't change col.
        """
        
        if row is not None:
            if row < 0:
                for i in range(-row):
                    self.__insert_new_row()
                    
                row = 0
                
            if row > self.__nlines:
                raise ValueError(f"Can't go that far in the history. ({row})")
                
            self.__cursor[0] = row
            
        if col is not None:
            if col < 0:
                col = 0
                
            self.__cursor[1] = col
            
    def __insert_new_row(self):
        self.__buffer.insert(0, ['',])
        
    def inline_erase(self, mode):
        # 0: erase from cursor to the end
        # 1: erase from beginning to cursor
        # 2: erase the line completely
        
        if mode == 0:
            self.__buffer[self.row] = self.__buffer[self.row][:self.col]
        elif mode == 1:
            for i in range(self.col):
                self.__buffer[self.row][i] = ' '
        elif mode == 2:
            self.__buffer[self.row] = ['',]
            
    def erase_display(self, mode=0):
        
        if mode == 0:
            self.__buffer = self.__buffer[self.row:]   
            self.move_absolute(0,0)
            
        else:
            logger.warn(f"Erase mode {mode} not implemented")
        
    
    def write(self, data):
        for d in data:
            if d == '\n':
                self.nl()
            elif d == '\r':
                self.cr()
            else:
                
                logger.debug((self.__cursor, len(self.__buffer)))
                
                if len(self.__buffer) <= self.row:
                    logger.warn(f"Missing {len(self.__buffer)-self.row} lines in buffer. Appending some, blank.")
                    
                    while len(self.__buffer) <= self.row:
                        self.__buffer += [['',],]
                
                while len(self.__buffer[self.row]) <= self.col:
                    self.__buffer[self.row] += [' ',]
                    
                self.__buffer[self.row][self.col] = d
                self.move_relative(col=1)
        
                
    def cr(self):
        """
        Perform a carriage return
        """
        self.move_absolute(col=0)
                
    def nl(self):
        """
        Create a new line if needed, jump to next line if not currently at last line. Move to the beginning of the line.
        """
        if self.row == 0:
            self.__insert_new_row()
        else:
            self.move_relative(row=-1)
            
        self.cr()        
        
    def getlines(self, onlyfinal=True):
        
        ll = []
        
        finalsize = self.__nlines if onlyfinal else 0
        
        while len(self.__buffer) > finalsize:
            ll += [ ''.join(self.__buffer.pop()) +'\n',]
            
        return ll

