#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 
# This file is part of the noansilogger software (https://gitlab.esrf.fr/chevremo/noansilogger/).
# Copyright (c) 2023 William Chèvremont <william.chevremont@esrf.fr>.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import time
import logging

from .ansi_interpreter import BufferizedNoANSIFile


logging.basicConfig(format='%(levelname)s %(name)s:%(lineno)d %(message)s',
                    handlers=[logging.StreamHandler(sys.stderr)])
logger = logging.getLogger(__name__)


def main():
    
    import argparse
    
    parser = argparse.ArgumentParser(description="Remove ANSI code from stdin and write result to file")
    
    parser.add_argument('-N', dest='bufsize', type=int, default=200, help="Maximum number of lines in buffer (default: %(default)s)")
    parser.add_argument('-v', dest='verbose', action='store_true', default=False, help='Verbose mode (minimum message level=debug)')
    parser.add_argument('--quiet', dest='quiet', action='store_true', default=False, help='Quiet mode (minimum message level=warning)')
    parser.add_argument('-a', dest='append', action='store_true', default=False, help='Do not truncate output file')
    parser.add_argument('--keep-colors', dest='keep_colors', action='store_true', default=False, help='Do not remove text formatting codes')
    parser.add_argument('-E', dest='noext', action='store_true', default=False, help='Do not append extension automatically')
    
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-D', dest='autodatetime', action='store_true', default=False, help='Append date and time automatically to file name')
    group.add_argument('-d', dest='autodate', action='store_true', default=False, help='Append date automatically to file name')
    
    
    parser.add_argument('output_file', action='store', help='Output filename')
    
    args = parser.parse_args()
            
    # Set the log level of each package according to the defined verbosity
    loglevel = logging.INFO
    
    if args.verbose:
        loglevel = logging.DEBUG
    elif args.quiet:
        loglevel = logging.WARNING
        
    for name in logging.root.manager.loggerDict:
        if name.startswith('noansilogger'):
            logging.getLogger(name).setLevel(loglevel)
            
    filename = os.path.basename(args.output_file)
    filename, ext = os.path.splitext(filename)
    path = os.path.abspath(os.path.dirname(args.output_file))

    if args.autodatetime:
        filename += time.strftime('_%Y%m%d_%H%M%S')
    elif args.autodate:
        filename += time.strftime('_%Y%m%d')
        
    if len(ext) == 0 and not args.noext:
        ext = '.log'    

    filename = path+'/'+filename+ext

    with BufferizedNoANSIFile(filename,args.append, args.bufsize ,args.keep_colors) as fd:
        for l in sys.stdin:
            fd.write(l)
    
            
            