# noansilogger

## Usage

Remove ANSI code from stdin and log to output file

```bash

usage: noansilogger [-h] [-N BUFSIZE] [-v] [--quiet] [-a] [--keep-colors] [-E] [-D | -d] output_file

Remove ANSI code from stdin and write result to file

positional arguments:
  output_file    Output filename

optional arguments:
  -h, --help     show this help message and exit
  -N BUFSIZE     Maximum number of lines in buffer (default: 200)
  -v             Verbose mode (minimum message level=debug)
  --quiet        Quiet mode (minimum message level=warning)
  -a             Do not truncate output file
  --keep-colors  Do not remove text formatting codes
  -E             Do not append extension automatically
  -D             Append date and time automatically to file name
  -d             Append date automatically to file name

```

## Install

```bash
pip install https://gitlab.esrf.fr/chevremo/noansilogger/-/archive/main/noansilogger-main.zip
```
